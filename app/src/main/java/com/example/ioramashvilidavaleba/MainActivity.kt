package com.example.ioramashvilidavaleba

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }


    private fun init() {
        button.setOnClickListener {
            d("button", "Button Activated")
            textview()
        }
    }

    private fun textview() {
        val number: Int = (-100..100).random()
        d("randomgeneratednumber", "$number")
        if (number % 5 == 0 ) {
            if ( number / 5 > 0 )
                textview.text = "Yes"
        }else{
            textview.text = "No"
        }

    }
}